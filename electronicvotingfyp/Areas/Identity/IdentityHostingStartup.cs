using System;
using electronicvotingfyp.Areas.Identity.Data;
using electronicvotingfyp.Controllers;
using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(electronicvotingfyp.Areas.Identity.IdentityHostingStartup))]
namespace electronicvotingfyp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<electronicvotingfypIdentityDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("electronicvotingfypIdentityDbContextConnection")));

                services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<electronicvotingfypIdentityDbContext>()
                    .AddDefaultTokenProviders();
                services.AddSignalR();

                services.AddRazorPages();

                services.AddTransient<IEmailSender, EmailSender>();


            });
        }

    }
}
