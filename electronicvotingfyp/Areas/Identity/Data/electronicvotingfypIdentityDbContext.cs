using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace electronicvotingfyp.Areas.Identity.Data;

public class electronicvotingfypIdentityDbContext : IdentityDbContext<IdentityUser>
{
    public electronicvotingfypIdentityDbContext(DbContextOptions<electronicvotingfypIdentityDbContext> options)
        : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("server=localhost,1433; database=tempdb;user=SA;password=someThingComplicated1234;MultipleActiveResultSets=true; Encrypt= false");
        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);
        builder.Entity<Dashboard>().HasOne(d => d.User);
        //builder.Entity<Election>().HasMany(e => e.CandidatesVoters);
        builder.Entity<Election>().HasOne(e => e.Creator);


        builder.Entity<Election>().HasMany(e => e.Dashboards)
            .WithMany(e => e.Elections)
            .UsingEntity<Participation>();

        builder.Entity<ElectionOption>().HasOne(o => o.Election);
        builder.Entity<ElectionKeySystem>().HasOne(ks => ks.RelatedElection);

        builder.Entity<BallotBox>().HasOne(b => b.electionKeys);
        builder.Entity<BallotBox>().HasMany(b => b.Ballots);

        //builder.Entity().

    }
    public DbSet<Dashboard> Dashboards { get; set; }
    public DbSet<Election> Elections { get; set; }
    public DbSet<Participation> Participations { get; set; }
    public DbSet<ElectionOption> Options { get; set; }
    public DbSet<ElectionKeySystem> ElectionKeys { get; set; }
    public DbSet<Ballot>Ballots { get; set; }
    public DbSet<BallotBox>BallotBoxes { get; set; }


}
