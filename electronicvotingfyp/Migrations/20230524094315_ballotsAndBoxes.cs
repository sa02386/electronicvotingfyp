﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace electronicvotingfyp.Migrations
{
    public partial class ballotsAndBoxes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BallotBoxes",
                columns: table => new
                {
                    BallotBoxId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AllowedVotes = table.Column<int>(type: "int", nullable: false),
                    ElectionKeySystemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BallotBoxes", x => x.BallotBoxId);
                    table.ForeignKey(
                        name: "FK_BallotBoxes_ElectionKeys_ElectionKeySystemId",
                        column: x => x.ElectionKeySystemId,
                        principalTable: "ElectionKeys",
                        principalColumn: "ElectionKeySystemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ballots",
                columns: table => new
                {
                    BallotId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    choiceCipher = table.Column<byte[]>(type: "varbinary(max)", nullable: false),
                    trackingCipher = table.Column<byte[]>(type: "varbinary(max)", nullable: false),
                    BallotBoxId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ballots", x => x.BallotId);
                    table.ForeignKey(
                        name: "FK_Ballots_BallotBoxes_BallotBoxId",
                        column: x => x.BallotBoxId,
                        principalTable: "BallotBoxes",
                        principalColumn: "BallotBoxId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BallotBoxes_ElectionKeySystemId",
                table: "BallotBoxes",
                column: "ElectionKeySystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Ballots_BallotBoxId",
                table: "Ballots",
                column: "BallotBoxId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ballots");

            migrationBuilder.DropTable(
                name: "BallotBoxes");
        }
    }
}
