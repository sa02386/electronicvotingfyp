﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace electronicvotingfyp.Migrations
{
    public partial class electionKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ElectionKeys",
                columns: table => new
                {
                    ElectionKeySystemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    publicKey = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    privateKey = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RelatedElectionElectionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElectionKeys", x => x.ElectionKeySystemId);
                    table.ForeignKey(
                        name: "FK_ElectionKeys_Elections_RelatedElectionElectionId",
                        column: x => x.RelatedElectionElectionId,
                        principalTable: "Elections",
                        principalColumn: "ElectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ElectionKeys_RelatedElectionElectionId",
                table: "ElectionKeys",
                column: "RelatedElectionElectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ElectionKeys");
        }
    }
}
