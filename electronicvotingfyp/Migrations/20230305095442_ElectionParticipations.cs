﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace electronicvotingfyp.Migrations
{
    public partial class ElectionParticipations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Elections_ElectionId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Elections_ElectionId1",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Elections_Dashboards_DashboardId",
                table: "Elections");

            migrationBuilder.DropIndex(
                name: "IX_Elections_DashboardId",
                table: "Elections");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ElectionId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ElectionId1",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DashboardId",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "ElectionId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ElectionId1",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "CreatorId",
                table: "Elections",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Elections",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PublicKey",
                table: "Elections",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Elections",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Elections",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "isVisible",
                table: "Elections",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Participations",
                columns: table => new
                {
                    ParticipationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    isCandidate = table.Column<bool>(type: "bit", nullable: false),
                    ElectionId = table.Column<int>(type: "int", nullable: false),
                    ParticipantId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DashboardId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participations", x => x.ParticipationId);
                    table.ForeignKey(
                        name: "FK_Participations_AspNetUsers_ParticipantId",
                        column: x => x.ParticipantId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Participations_Dashboards_DashboardId",
                        column: x => x.DashboardId,
                        principalTable: "Dashboards",
                        principalColumn: "DashboardId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Participations_Elections_ElectionId",
                        column: x => x.ElectionId,
                        principalTable: "Elections",
                        principalColumn: "ElectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Elections_CreatorId",
                table: "Elections",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Participations_DashboardId",
                table: "Participations",
                column: "DashboardId");

            migrationBuilder.CreateIndex(
                name: "IX_Participations_ElectionId",
                table: "Participations",
                column: "ElectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Participations_ParticipantId",
                table: "Participations",
                column: "ParticipantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Elections_AspNetUsers_CreatorId",
                table: "Elections",
                column: "CreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Elections_AspNetUsers_CreatorId",
                table: "Elections");

            migrationBuilder.DropTable(
                name: "Participations");

            migrationBuilder.DropIndex(
                name: "IX_Elections_CreatorId",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "PublicKey",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Elections");

            migrationBuilder.DropColumn(
                name: "isVisible",
                table: "Elections");

            migrationBuilder.AddColumn<int>(
                name: "DashboardId",
                table: "Elections",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ElectionId",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ElectionId1",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Elections_DashboardId",
                table: "Elections",
                column: "DashboardId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ElectionId",
                table: "AspNetUsers",
                column: "ElectionId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ElectionId1",
                table: "AspNetUsers",
                column: "ElectionId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Elections_ElectionId",
                table: "AspNetUsers",
                column: "ElectionId",
                principalTable: "Elections",
                principalColumn: "ElectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Elections_ElectionId1",
                table: "AspNetUsers",
                column: "ElectionId1",
                principalTable: "Elections",
                principalColumn: "ElectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Elections_Dashboards_DashboardId",
                table: "Elections",
                column: "DashboardId",
                principalTable: "Dashboards",
                principalColumn: "DashboardId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
