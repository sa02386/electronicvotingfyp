﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace electronicvotingfyp.Migrations
{
    public partial class electionOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PublicKey",
                table: "Elections");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Elections",
                type: "varchar(64)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "Question",
                table: "Elections",
                type: "varchar(128)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Options",
                columns: table => new
                {
                    ElectionOptionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OptionName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ChoiceCount = table.Column<int>(type: "int", nullable: false),
                    ElectionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Options", x => x.ElectionOptionId);
                    table.ForeignKey(
                        name: "FK_Options_Elections_ElectionId",
                        column: x => x.ElectionId,
                        principalTable: "Elections",
                        principalColumn: "ElectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Options_ElectionId",
                table: "Options",
                column: "ElectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Options");

            migrationBuilder.DropColumn(
                name: "Question",
                table: "Elections");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Elections",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(64)");

            migrationBuilder.AddColumn<string>(
                name: "PublicKey",
                table: "Elections",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
