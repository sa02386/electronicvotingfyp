﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace electronicvotingfyp.Migrations
{
    public partial class participationsDashBoardsElections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participations_AspNetUsers_ParticipantId",
                table: "Participations");

            migrationBuilder.DropForeignKey(
                name: "FK_Participations_Dashboards_DashboardId",
                table: "Participations");

            migrationBuilder.DropForeignKey(
                name: "FK_Participations_Elections_ElectionId",
                table: "Participations");

            migrationBuilder.DropIndex(
                name: "IX_Participations_ParticipantId",
                table: "Participations");

            migrationBuilder.DropColumn(
                name: "ParticipantId",
                table: "Participations");

            migrationBuilder.DropColumn(
                name: "isCandidate",
                table: "Participations");

            migrationBuilder.RenameColumn(
                name: "ElectionId",
                table: "Participations",
                newName: "ElectionId1");

            migrationBuilder.RenameColumn(
                name: "DashboardId",
                table: "Participations",
                newName: "DashboardId1");

            migrationBuilder.RenameIndex(
                name: "IX_Participations_ElectionId",
                table: "Participations",
                newName: "IX_Participations_ElectionId1");

            migrationBuilder.RenameIndex(
                name: "IX_Participations_DashboardId",
                table: "Participations",
                newName: "IX_Participations_DashboardId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Participations_Dashboards_DashboardId1",
                table: "Participations",
                column: "DashboardId1",
                principalTable: "Dashboards",
                principalColumn: "DashboardId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Participations_Elections_ElectionId1",
                table: "Participations",
                column: "ElectionId1",
                principalTable: "Elections",
                principalColumn: "ElectionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participations_Dashboards_DashboardId1",
                table: "Participations");

            migrationBuilder.DropForeignKey(
                name: "FK_Participations_Elections_ElectionId1",
                table: "Participations");

            migrationBuilder.RenameColumn(
                name: "ElectionId1",
                table: "Participations",
                newName: "ElectionId");

            migrationBuilder.RenameColumn(
                name: "DashboardId1",
                table: "Participations",
                newName: "DashboardId");

            migrationBuilder.RenameIndex(
                name: "IX_Participations_ElectionId1",
                table: "Participations",
                newName: "IX_Participations_ElectionId");

            migrationBuilder.RenameIndex(
                name: "IX_Participations_DashboardId1",
                table: "Participations",
                newName: "IX_Participations_DashboardId");

            migrationBuilder.AddColumn<string>(
                name: "ParticipantId",
                table: "Participations",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isCandidate",
                table: "Participations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Participations_ParticipantId",
                table: "Participations",
                column: "ParticipantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Participations_AspNetUsers_ParticipantId",
                table: "Participations",
                column: "ParticipantId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Participations_Dashboards_DashboardId",
                table: "Participations",
                column: "DashboardId",
                principalTable: "Dashboards",
                principalColumn: "DashboardId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Participations_Elections_ElectionId",
                table: "Participations",
                column: "ElectionId",
                principalTable: "Elections",
                principalColumn: "ElectionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
