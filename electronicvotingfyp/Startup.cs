﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace electronicvotingfyp
{
    public class Startup
    {
        private IConfiguration _config;

        public Startup(IConfiguration config)
        {
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<AppDBContext>(options =>options.UseSqlServer(_config.GetConnectionString("UserDBConnection")));
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDBContext>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            // For more information on how to configure your application, visit https://go.microsoft.co m/fwlink/?LinkID=398940
        }
    }
}

