﻿using System;
namespace electronicvotingfyp.Models
{
	public class CreateBallotViewModel
	{
		public List<ElectionOption> options { get; set; }
		public Election elect { get; set; }
		public string choice { get; set; }
        public string tracking { get; set; }

    }
}

