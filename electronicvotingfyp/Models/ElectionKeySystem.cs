﻿using System;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace electronicvotingfyp.Models
{
	public class ElectionKeySystem
	{

		public int ElectionKeySystemId { get; set; }

		public string publicKey { get; set; }

		public string privateKey { get; set; }

		public Election RelatedElection { get; set; }

	}
}

