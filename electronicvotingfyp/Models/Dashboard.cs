﻿using System;
using Microsoft.AspNetCore.Identity;

namespace electronicvotingfyp.Models
{
	public class Dashboard
	{
		public int DashboardId { get; set; }

		//navigation properties
		public IdentityUser User { get; set; }

		public ICollection<Election> Elections { get; set; }

		public ICollection<Participation> Participations { get; set;}
	}
}

