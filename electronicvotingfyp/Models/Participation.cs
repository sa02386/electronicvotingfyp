﻿using System;
using Microsoft.AspNetCore.Identity;

namespace electronicvotingfyp.Models
{
	public class Participation
	{
		public int ParticipationId { get; set; }

		public int ElectionId;

		public int DashboardId;
		//navigation properties
		public Election Election { get; set; } = null!;

		// public IdentityUser Participant { get; set; }

        public Dashboard Dashboard { get; set; } = null!;
    }
}
