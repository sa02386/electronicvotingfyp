﻿using System;
namespace electronicvotingfyp.Models
{
	public class Ballot
	{
		public int BallotId { get; set; }

		public byte[] choiceCipher { get; set; }

		public byte[] trackingCipher { get; set; }

		//public object signature { get; set; }

		//public object verificationKey { get; set; }

		public BallotBox box;
	}
}

