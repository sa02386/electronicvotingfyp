﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace electronicvotingfyp.Models
{
	public class Election
	{
		public int ElectionId { get; set; }

		public byte[]? ElectionPublicKey;

		[Required]
		[Column(TypeName = "varchar(64)")]
        public string Title {
			get;

			set;
		}
		[Required]
        [Column(TypeName = "varchar(128)")]
        public string Question {
			get;

			set;
		}
		
		[Required]
		public Boolean isVisible { get; set; }

		[Required]
		public DateTime StartDate { get; set; }

		[Required]
        public DateTime EndDate { get; set; }

		//navigation properties

        public ICollection<Participation> Participations{ get; set; }

		public ICollection<Dashboard> Dashboards { get; set; }

        public ICollection<ElectionOption> Options { get; set; }

		public IdentityUser Creator { get; set; }


	}
}