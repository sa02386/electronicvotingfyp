﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace electronicvotingfyp.Models
{
	public class ElectionCreateViewModel
	{
		public int ElectionId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Election Title")]
        public string Title { get; set; }

        [Required]
		[DataType(DataType.DateTime)]
		[Display(Name = "Election Start Date")]
        [DisplayFormat(DataFormatString = "{0:yyy-MM-ddTHH:mm}")]
        public DateTime StartDate { get; set; }

		[Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Election End Date")]
		[DisplayFormat(DataFormatString ="{0:yyy-MM-ddTHH:mm}")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name ="Election manifest")]
        public string Question { get; set; }

        public List<string> VotingOptions { get; set;}

        public List<string> Voters { get; set; }

        [BindProperty]
        [Required]
        [Display(Name = "Candidate Options")]
        public IFormFile VotingOptionsFile { get; set; }

        [BindProperty]
        [Required]
        [Display(Name = "Authorised Voters")]
        public IFormFile VotersFile { get; set; }

        [Required]
        [Display(Name = "Make Election Visible")]
        public Boolean IsVisible { get; set; }

        public Boolean validInputs { get; set; }

		public IEnumerable<IdentityUser> SystemUsers;

		public IEnumerable<SelectListItem> GetAuthorisedUsers(IEnumerable<IdentityUser> SystemUsers)
		{
			return new SelectList(SystemUsers, "Id", "UserName");
		}
    }
}

