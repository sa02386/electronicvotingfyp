﻿using System;
namespace electronicvotingfyp.Models
{
	public class ElectionOption
	{
		public int ElectionOptionId { get; set; }
           
		public string OptionName { get; set; }

        public int ChoiceCount { get; set; }

        //navigation preoperties

        public Election Election { get; set; }
    }

}

