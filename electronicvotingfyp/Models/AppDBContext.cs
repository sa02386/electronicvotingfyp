﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace electronicvotingfyp.Models
{
	public class AppDBContext : IdentityDbContext 
	{
		public AppDBContext(DbContextOptions<AppDBContext> options): base(options)
		{

		}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}

