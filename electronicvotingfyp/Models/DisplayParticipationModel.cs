﻿using System;
namespace electronicvotingfyp.Models
{
	public class DisplayParticipationModel
	{
		public List<Election> participations { get; set; }
		public List<Election> creations { get; set; }
	}
}

