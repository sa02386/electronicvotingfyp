﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
namespace electronicvotingfyp.Models
{
	public class FileUploadChecker
	{
        public string ErrorMessage { get; set; }

        public decimal filesize { get; set; }

        public Boolean validFile = false;

        public Boolean UploadUserFile(IFormFile file)
        {
            try
            {
                var supportedTypes = new[] { "txt", "xls", "xlsx","csv" };
                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                if (!supportedTypes.Contains(fileExt))
                {
                    ErrorMessage = "File Extension Is InValid - Only Upload CSV/EXCEL/TXT File";
                    validFile = false;
                    return validFile;
                }
                else
                {
                    ErrorMessage = "File Is Successfully Uploaded";
                    validFile = true;
                    return validFile;

                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "Upload Container Should Not Be Empty or Contact Admin";
                validFile = false;
                return validFile;
            }
        }


        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and pr ocess domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }



        //checks the content of the emails csv file and stores it 
        public  List<string> AddVoters(List<string> Voters, IFormFile file)
        {
            using var fileReader = new StreamReader(file.OpenReadStream());

            { string email = null;
                while (fileReader.Peek() >= 0)
                {
                    email = fileReader.ReadLine();
                    if (IsValidEmail(email))
                    {
                        Voters.Add(email);
                    }
                    else {
                        ErrorMessage = "Invalid email entered please recheck voters list";
                        validFile = false;
                        break; }
                }
            }

            return Voters;
        }
        // talk about coding practices like use async try catch in report style cop
        public List<string> AddOptions(List<string> Options, IFormFile file)
        {
            if(validFile)
            {
                using var fileReader = new StreamReader(file.OpenReadStream());
                string option = null;
                while(fileReader.Peek() >= 0)
                {
                    option = fileReader.ReadLine();
                    if(option.Length > 0 && !option.Contains("'") && !option.Contains("--") && !option.Contains("/*") & !option.Contains("*/") & !option.Contains("xp_") &&option.Length <= 32)
                    {
                        Options.Add(option);
                    }
                    else
                    {
                        ErrorMessage = "Invalid option forat please recheck options list";
                        validFile = false;
                        break;
                    }

                }
            }


            return Options;
        }

        //Possibly use to prevent large files
        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    var options = ((ElectionCreateViewModel)validationContext.ObjectInstance).VotingOptionsFile;
        //    var extension = Path.GetExtension(options.FileName);
        //    var size = options.Length;

        //    if (!extension.ToLower().Equals(".csv"))
        //    {
        //        this.validInputs = false;
        //        yield return new ValidationResult("File extension is not valid.");
        //    }

        //    if (size > (5 * 1024 * 1024))
        //    {
        //        yield return new ValidationResult("File size is bigger than 5MB.");
        //    }
        //}
    }
}

