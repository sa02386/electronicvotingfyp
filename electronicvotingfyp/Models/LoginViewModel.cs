﻿using System;
using System.ComponentModel.DataAnnotations;

namespace electronicvotingfyp.Models
{
	public class LoginViewModel
	{
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

