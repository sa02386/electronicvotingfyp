﻿using System;
namespace electronicvotingfyp.Models
{
	public class BallotBox
	{
		public int BallotBoxId { get; set; }

		public int AllowedVotes { get; set; }

        public ElectionKeySystem electionKeys { get; set; }

        public List<Ballot> Ballots { get; set; }

    }
}

