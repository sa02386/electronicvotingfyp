﻿using System;
namespace electronicvotingfyp.Controllers
{
	public class BallotBoxController
	{
		private List<byte[]> BallotsCasted;
		public BallotBoxController()
		{
			this.BallotsCasted = new List<byte[]>();
		}

		public void ReceiveBallot(byte[] ballot)
		{
			this.BallotsCasted.Add(ballot);
		}
	}
}

