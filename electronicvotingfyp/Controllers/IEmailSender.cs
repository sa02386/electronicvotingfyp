﻿using System;
namespace electronicvotingfyp.Controllers
{
	public interface IEmailSender
	{
		Task SendEmailAsync(string receiver, string mailSubject, string mailContent);
	}
}

