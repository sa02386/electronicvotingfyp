﻿using System;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace electronicvotingfyp.Controllers
{
	public class ElectionKeySystemContrroller
	{
		public AsymmetricCipherKeyPair electionKeys;

		public ElGamalParameters encryptionParameters;

		public ElGamalEngine engine;

		public ElectionKeySystemContrroller(ElGamalPublicKeyParameters pkParams)
		{
			this.engine = this.StartElgamalEngine();
			this.encryptionParameters = pkParams.Parameters;
            electionKeys = this.GenerateKeys(encryptionParameters);
            //ElGamalKeyParameters keypam = (ElGamalKeyParameters)pkParams.Public;
            //this.encryptionParameters = keypam.Parameters;
            engine.Init(true, pkParams);

        }

		public ElectionKeySystemContrroller()
		{
			encryptionParameters = this.GenerateParameters();
			electionKeys = this.GenerateKeys(encryptionParameters);
			engine = this.StartElgamalEngine();
			//engine.Init(true, electionKeys.Public);


        }

		public ElGamalParameters GenerateParameters()
		{
            ElGamalParametersGenerator generator = new ElGamalParametersGenerator();
            SecureRandom rnd = new SecureRandom();
            generator.Init(1024, 1, rnd);
            ElGamalParameters parameters = generator.GenerateParameters();

			return parameters;
        }

		public AsymmetricCipherKeyPair GenerateKeys( ElGamalParameters gamalParameters)

		{


		ElGamalKeyGenerationParameters keyGenerationParameters = new ElGamalKeyGenerationParameters(new SecureRandom(),gamalParameters);
		ElGamalKeyPairGenerator keyGenerator = new ElGamalKeyPairGenerator();
		keyGenerator.Init(keyGenerationParameters);
		AsymmetricCipherKeyPair electionKeys = keyGenerator.GenerateKeyPair();


			return electionKeys;

        }

		public ElGamalEngine StartElgamalEngine()
		{
			ElGamalEngine engine = new ElGamalEngine();
			return engine;
		}

		public byte[] Encrypt(byte[] plainBallot, int inOff, int length)
		{

			return this.engine.ProcessBlock(plainBallot, inOff, length);
		}

		public void SendBallot(BallotBoxController ballotBox, byte[]cipher)
		{
			ballotBox.ReceiveBallot(cipher);
		}
	}
}

