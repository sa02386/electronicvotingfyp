﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using electronicvotingfyp.Areas.Identity.Data;
using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace electronicvotingfyp.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

    }
}

