﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using electronicvotingfyp.Areas.Identity.Data;
using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.CodeAnalysis.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.X509;

//dotnet ef migrations add electionOptions --context electronicvotingfypIdentityDbContext
//dotnet ef database update --context electronicvotingfypIdentityDbContext
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace electronicvotingfyp.Controllers
{
    public class ElectionController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ILogger<AccountController> logger;

        public ElectionController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, ILogger<AccountController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Create()
        {
            ElectionCreateViewModel objElectModel = new ElectionCreateViewModel() { StartDate = DateTime.Now, EndDate = DateTime.Now.AddHours(24), SystemUsers = userManager.Users.AsEnumerable(), Question = "Which ones of these options do you choose?" , VotingOptions = new List<string>(), Voters = new List<string>() };

            return View(objElectModel);
        }
        [HttpGet]
        public IActionResult Vote()
        {
            return View();
        }
        [HttpGet]
        public IActionResult ManageElection()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ElectionCreateViewModel model, IFormFile    Candidates, IFormFile Voters)
        {
            
            FileUploadChecker fc = new FileUploadChecker();
            if (Candidates != null && Voters != null)
            {
                if (fc.UploadUserFile(Voters))
                {
                    fc.AddVoters(model.Voters, Voters);
                }

                if (fc.UploadUserFile(Candidates))
                {
                    List<string> Opt = new List<string>();
                    fc.AddOptions(Opt, Candidates);
                    model.VotingOptions = Opt;
                }
            }

                if (!fc.validFile)
                {
                TempData["Message"] = fc.ErrorMessage;

                return RedirectToAction("Create", "Election");
                }
                else
                {
                    DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
                    using (var db = new electronicvotingfypIdentityDbContext(options))
                    {
                    var user = await userManager.GetUserAsync(this.User);
                    var participations = new List<Dashboard>();

                    foreach (string uid in model.Voters)
                        {
                            var participant = await userManager.FindByEmailAsync(uid);
                        var board = db.Dashboards.Single(d => d.User.Id == participant.Id);

                        participations.Add(board);
                        }
                    var votingOptions = new List<ElectionOption>();
                    foreach (string option in model.VotingOptions)
                    {
                        ElectionOption electionOption = new ElectionOption { OptionName = option };
                        
                        votingOptions.Add(electionOption);

                    }

                    db.Elections.Add(new Election {Title = model.Title, Creator = db.Users.Single(u => u.Email == user.Email), Dashboards = participations, StartDate = model.StartDate, EndDate = model.EndDate, Question = model.Question, Options = votingOptions });
                    await db.SaveChangesAsync();

                    ElectionKeySystemContrroller electionAuthority = new ElectionKeySystemContrroller();

                    TextWriter tw = new StringWriter();
                    PemWriter pemWriter = new PemWriter(tw);
                    pemWriter.WriteObject(electionAuthority.electionKeys.Public);
                    pemWriter.Writer.Flush();
                    string pem_privatekey = tw.ToString();

                    pemWriter.WriteObject(electionAuthority.electionKeys.Private);
                    pemWriter.Writer.Flush();
                    string pem_publicKey = tw.ToString();
                    
                    ElectionKeySystem keys = new ElectionKeySystem() { RelatedElection = db.Elections.First(u => u.Creator == user && u.Title == model.Title), privateKey = pem_privatekey, publicKey = pem_publicKey };
                    BallotBox box = new BallotBox() { electionKeys = keys};


                    db.ElectionKeys.Add(keys);
                    db.BallotBoxes.Add(box);

                    await db.SaveChangesAsync();

                        return RedirectToAction("Index", "Dashboard");
                    }
                }
         }

     }
}


