﻿using System.Net;
using System.Net.Mail;
using electronicvotingfyp.Controllers;

public class EmailSender : IEmailSender
{
    public Task SendEmailAsync(string email, string subject, string message)
    {
        var client = new SmtpClient("smtp.office365.com", 587)
        {
            EnableSsl = true,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential("sa02386@surrey.ac.uk", "LAbelledu23")
        };

        return client.SendMailAsync(
            new MailMessage(from: "sa02386@surrey.ac.uk",
                            to: email,
                            subject,
                            message
                            ));
    }
}