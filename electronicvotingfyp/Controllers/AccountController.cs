﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using electronicvotingfyp.Areas.Identity.Data;
using electronicvotingfyp.Areas.Identity.Pages.Account.Manage;
using electronicvotingfyp.Migrations;
using electronicvotingfyp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using static System.Collections.Specialized.BitVector32;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace electronicvotingfyp.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ILogger<AccountController> logger;
        private readonly IEmailSender mailingService;
        //private readonly UrlEncoder encoder;
        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, ILogger<AccountController>logger, IEmailSender mailing
            )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
            this.mailingService = mailing;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Manage()
        {
            return View();
        }
        [HttpGet]
        public IActionResult EnableAuthenticator()
        {

            //EnableAuthenticatorModel model = new EnableAuthenticatorModel(this.userManager,this.authenthicathionLog , new UrlEncoder());
            var model = new EnableAuthenticatorModel();
            return View("/Areas/Identity/Pages/Account/Manage/EnableAuthenticator.cshtml", model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
             if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password,false, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("index", "dashboard");

                }
                
                ModelState.AddModelError(string.Empty, "Invalid credentials or account locked Please request for your account to be unlocked");
                
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                    var confirmationLink = Url.Action("ConfimEmail", "Account", new { userId = user.Id, token = token }, Request.Scheme);
                    logger.Log(LogLevel.Warning, confirmationLink);
                    DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
                    using (var db = new electronicvotingfypIdentityDbContext(options))
                    {
                        var Id = Guid.NewGuid();
                        Dashboard board = new Dashboard();
                        board.User = db.Users.Single(u => u.Email == user.Email);
                        db.Dashboards.Add(board);
                        await db.SaveChangesAsync();
                    }
                    //await signInManager.SignInAsync(user, isPersistent: false);
                    if (confirmationLink != null) { await mailingService.SendEmailAsync(model.Email, "Choose Confirmation Link", confirmationLink); }
                    return RedirectToAction("confirmation", "home");
                }
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfimEmail(string userId, string token)
        {
            if(userId == null||token == null)
            {
                return RedirectToAction("index", "home");
            }
            var user = await userManager.FindByIdAsync(userId);
             if(user == null)
            {
                ViewBag.ErrorMessage = "Invalid User";
                return View("NotFound");
            }

            var result = await userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                var unlockUser = await userManager.SetLockoutEnabledAsync(user,false);
                if (unlockUser.Succeeded)
                {
                    return RedirectToAction("login", "account");
                }
            }
            ViewBag.ErrorTitle = "Cannot confrm email";
            return View("Error");
        }
        [HttpPost]
        public async Task<ActionResult> Logout()
        {
           await signInManager.SignOutAsync();
            return RedirectToAction("index", "home");
        }

        public void ListUsers()
        {
            var users = userManager.Users.AsEnumerable();
            
        }


        public List<Election> GetElections(IdentityUser user)
        {
            List<Election> elections = new List<Election>();
            DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
            using (var db = new electronicvotingfypIdentityDbContext(options))
            {

                var board = db.Dashboards.Single(u => u.User.Id == user.Id);
                elections = db.Elections.Where(p => p.Dashboards.Contains(board)).ToList();
            }

            return elections;
        }
        public async Task<ViewResult> DisplayParticipations()
        {
            var user = await userManager.GetUserAsync(this.User);
            DisplayParticipationModel display = new DisplayParticipationModel { participations = this.GetElections(user), creations = this.GetCreatedElections(user) };
            return View(display);
        }

        public List<Election> GetCreatedElections(IdentityUser user)
        {
            List<Election> elections = new List<Election>();
            DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
            using (var db = new electronicvotingfypIdentityDbContext(options))
            {

                elections = db.Elections.Where(e => e.Creator == user).ToList();
            }
            return elections;
        }
        [HttpGet]
        public async Task<IActionResult> ViewCreated()
        {
            var user = await userManager.GetUserAsync(this.User);
            ViewCreatedViewModel vm = new ViewCreatedViewModel() { Created = this.GetCreatedElections(user) };
            return View();
        }
        [HttpGet]
        public IActionResult Tally()
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public IActionResult CreateBallot(int ElectionId)
        {

            List<ElectionOption> electionOptions = new List<ElectionOption>();
            Election election = null;
            DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
            using (var db = new electronicvotingfypIdentityDbContext(options))
            {

                election = db.Elections.Single(u => u.ElectionId == ElectionId);
                electionOptions  = db.Options.Where(p => p.Election  == election).ToList();
            }

            CreateBallotViewModel cbm = new CreateBallotViewModel() { options = electionOptions ,elect = election };

            return View(cbm);
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateBallot(CreateBallotViewModel cbm)
        {
            ElectionKeySystem keys = null;
            DbContextOptions<electronicvotingfypIdentityDbContext> options = new DbContextOptions<electronicvotingfypIdentityDbContext>();
            using (var db = new electronicvotingfypIdentityDbContext(options))
            {

                keys = db.ElectionKeys.Single(u => u.RelatedElection.ElectionId == cbm.elect.ElectionId);


                PemReader pr = new PemReader(new StringReader(keys.publicKey));
                ElGamalPublicKeyParameters keyPair = (ElGamalPublicKeyParameters)pr.ReadObject();
                //ElGamalPublicKeyParameters publicKeyParams = (ElGamalPublicKeyParameters)keyPair.Public;

                ElectionKeySystemContrroller ks = new ElectionKeySystemContrroller(keyPair);
                byte[] inputChoice = Encoding.UTF8.GetBytes(cbm.choice);
                byte[] inputTracking = Encoding.UTF8.GetBytes(cbm.tracking);
                byte[] encryptedChoice = ks.Encrypt(inputChoice, 0, inputChoice.Length);
                byte[] encryptedTracking = ks.Encrypt(inputTracking, 0, inputChoice.Length);

                Ballot ballot = new Ballot { choiceCipher = encryptedChoice, trackingCipher = encryptedTracking, box = db.BallotBoxes.Single(b => b.electionKeys.ElectionKeySystemId == keys.ElectionKeySystemId) };

                db.Ballots.Add(ballot);

                await db.SaveChangesAsync();

            }

            return View(cbm);
        }
    }
}

